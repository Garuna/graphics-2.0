#include "main.h"

#ifndef Heal_H
#define Heal_H


class Heal {
public:
    Heal() {}
    Heal(float x, float y,float z, color_t color);
    glm::vec3 position;
    float rotation;
    float rotation1;
    void draw(glm::mat4 VP);
    void set_position(float x, float y ,float z);
    void tick();
    double speed;
    double speedy ;
    double level;
    bool existence;
    bounding_box_t bounding_box();
    double health;
private:
    VAO *object;
    VAO * object1;
    VAO * object2;
    VAO * object3;

};

#endif // Heal_H
