#include "main.h"

const color_t COLOR_RED = { 236, 100, 75 };
const color_t COLOR_GREEN = { 135, 211, 124 };
const color_t COLOR_BLACK = { 52, 73, 94 };
const color_t COLOR_BROWN = {160, 82, 45};
const color_t COLOR_BACKGROUND = { 242, 241, 239 };
const color_t COLOR_YELLOW = { 255,215,0};
const color_t COLOR_PURPLE = {138,43,226};
const color_t COLOR_FIRE = {255,0,0};
const color_t COLOR_PINK = {255,105,180};
const color_t COLOR_BLUE = { 0,191,255 };
const color_t COLOR_DEEP_BLUE = { 0,101,255 };
const color_t COLOR_LIME = { 1, 225, 14 };
const color_t COLOR_BOOST = {138,43,226 };
