#include "main.h"

#ifndef Barrel_H
#define Barrel_H


class Barrel {
public:
    Barrel() {}
    Barrel(float x, float y,float z, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y ,float z);
    void tick();
    double speed;
    double level;
    bool existence;
    bounding_box_t bounding_box();
    double health;
private:
    VAO *object;
    VAO * object1;
    VAO * object2;
    VAO * object3;

};

#endif // Barrel_H
