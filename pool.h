#include "main.h"

#ifndef Pool_H
#define Pool_H


class Pool {
public:
    Pool() {}
    Pool(float x, float y,float z, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y , float z);
    void tick();
    double speed;
    bool existence;
    double level;
private:
    VAO *object;
    VAO * object1;
    VAO * object2;
    VAO * object3;

};

#endif // Pool_H
