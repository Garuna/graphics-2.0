#include "main.h"
#include "timer.h"
#include "boat.h"
#include "rock.h"
#include "pool.h"
#include "barrel.h"
#include "star.h"
#include "enemy.h"
#include "fireball.h"
#include "boss.h"
#include "heal.h"
#include "boost.h"

using namespace std;

GLMatrices Matrices;
GLuint     programID;
GLFWwindow *window;

/**************************
* Customizable functions *
**************************/
char output[1000000];
Boost boost[2];
Boat boat1;
Rock rock[50];
Pool pool;
Barrel barrel[12];
Star star[12];
Enemy enemy[4];
Boss boss;
Fireball fireball;
Fireball fireboat1;
Heal heal[4];

bool isup = 0;
bool bossflag = 0;
bool bossflag1 = 0;
bool wind = 1;
bool fire = 0;
bool rock_collision = 0;
bool boostflag[2];

float t;
int score = 0;
float target_x = 0;
float target_y = 0;
float target_z = 0;

float camera_x = 0 ;
float camera_y = 0;
float camera_z = 0;

bool pressed = 0;
int view = 0;
float screen_zoom = 1, screen_center_x = 0, screen_center_y = 0;
float camera_rotation_angle = 0;
bool enemyflag[4] = {0};
bool fire_boss = 0;

Timer t60(1.0 / 60);
GLint i = 0;
/* Render the scene with openGL */
/* Edit this function according to your assignment */
void draw() {
    // clear the color and depth in the frame buffer
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the loaded shader program
    // Don't change unless you know what you are doing
    glUseProgram (programID);

    // Eye - Location of camera. Don't change unless you are sure!!
//    glm::vec3 eye ( 5*cos(camera_rotation_angle*M_PI/180.0f), 0,  5*sin(camera_rotation_angle*M_PI/180.0f) );
//        glm::vec3 eye (-100 , 111 ,15 );


//    glm::vec3 eye (boat1.position.x  , boat1.position.y +15 , boat1.position.z + 10);
//    if (view == 0)
//    {
//        glm::vec3 eye (boat1.position.x + 15*sin(boat1.rotation*M_PI/180)  , 33 , boat1.position.z +15*cos(boat1.rotation*M_PI/180));

//        // Target - Where is the camera looking at.  Don't change unless you are sure!!
//        glm::vec3 target (boat1.position.x, boat1.position.y, boat1.position.z);
//        // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
//        glm::vec3 up (0, 1, 0);
//        Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D

//    }
//    else if (view == 1)
//    {
//        glm::vec3 eye (boat1.position.x  , 102, boat1.position.z + 2 );

//        // Target - Where is the camera looking at.  Don't change unless you are sure!!
//        glm::vec3 target (boat1.position.x, boat1.position.y, boat1.position.z);
//        // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
//        glm::vec3 up (0, 1, 0);
//        Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D

//    }
//    else if (view == 2)
//    {
//        glm::vec3 eye (boat1.position.x - 20  , 102, boat1.position.z + 2 );

//        // Target - Where is the camera looking at.  Don't change unless you are sure!!
//        glm::vec3 target (boat1.position.x, boat1.position.y, boat1.position.z);
//        // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
//        glm::vec3 up (0, 1, 0);
//        Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D

//    }
//    else if (view == 3)
//    {
    glm::vec3 eye (camera_x  , camera_y , camera_z);

    // Target - Where is the camera looking at.  Don't change unless you are sure!!
    glm::vec3 target (target_x  , target_y , target_z);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    glm::vec3 up (0, 1, 0);
    Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D

//    }

//    glm::vec3 eye (2 , -4 ,0 );

    // Compute Camera matrix (view)
    // Don't change unless you are sure!!
//    Matrices.view = glm::lookAt(glm::vec3(0, 0, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)); // Fixed camera for 2D (ortho) in XY plane

    // Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
    // Don't change unless you are sure!!
    glm::mat4 VP = Matrices.projection * Matrices.view;

    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // For each model you render, since the MVP will be different (at least the M part)
    // Don't change unless you are sure!!
    glm::mat4 MVP;  // MVP = Projection * View * Model

    // Scene render
    pool.draw(VP);
    for (i =0 ; i < 50 ; i++)
       if (rock[i].existence)
       rock[i].draw(VP);

       if(boat1.existence)
        boat1.draw(VP);
       for (i=0;i<12;i++)
       if(star[i].existence)
        {
           star[i].draw(VP);
           barrel[i].draw(VP);
        }
       for(i = 0 ; i < 4 ; i++)
         if (enemy[i].existence)
            enemy[i].draw(VP);
       if (fireball.existence)
           fireball.draw(VP);
       if (bossflag && boss.existence)
       {
           boss.draw(VP);
           fireboat1.draw(VP);
       }
       for(i=0 ; i< 4 ; i++)
         if(heal[i].existence)
               heal[i].draw(VP);
       if(boost[0].existence)
           boost[0].draw(VP);
       if(boost[1].existence)
           boost[1].draw(VP);

}
void tick_input(GLFWwindow *window) {
    int left  = glfwGetKey(window, GLFW_KEY_LEFT);
    int right = glfwGetKey(window, GLFW_KEY_RIGHT);
    int up  = glfwGetKey(window, GLFW_KEY_UP);
    int down = glfwGetKey(window, GLFW_KEY_DOWN);
    int space = glfwGetKey(window, GLFW_KEY_SPACE);
    int a = glfwGetKey(window, GLFW_KEY_A);
    int d = glfwGetKey(window, GLFW_KEY_D);
    int f = glfwGetKey(window, GLFW_KEY_F);
    int v = glfwGetKey(window, GLFW_KEY_V);




    if (left) {
//        camera_rotation_angle +=1;
//        boat1.position.x = boat1.position.x - .5;
        boat1.rotation += 5;
        pool.rotation += 5;
//        pool.position.x = pool.position.x - .5;

        // Do something
    }

    if (right){
//        camera_rotation_angle +=-1;
//    screen_zoom = screen_zoom + 2;
//        boat1.position.x = boat1.position.x + .5;
//        boat1.rotation = -4;
//        pool.position.x = pool.position.x + .5;
            boat1.rotation -= 5;
            pool.rotation -= 5;



    }
    if(v && !pressed)
    {
//          view +=1;
//          view = view - (view/4)*4;
        camera_change();
        pressed = 1;
    }
    else
        pressed = 0;

    if (a){
//        camera_rotation_angle +=-1;
//    screen_zoom = screen_zoom + 2;
        if(boat1.position.x > -50)
{
        boat1.position.x = boat1.position.x - .5;
//        boat1.rotation = -4;
        pool.position.x = pool.position.x - .5;
}
        //            boat1.rotation -= 5;
//            pool.rotation -= 5;



    }


    if (d){
//        camera_rotation_angle +=-1;
//    screen_zoom = screen_zoom + 2;
        if(boat1.position.x < 50)
 {
        boat1.position.x = boat1.position.x + .5;

        //        boat1.rotation = -4;
        pool.position.x = pool.position.x + .5;
    }
//            boat1.rotation -= 5;
//            pool.rotation -= 5;



    }

    if (up){
//        camera_rotation_angle +=-1;
//    screen_zoom = screen_zoom + 2;
    if(boat1.position.x > -50)
    {

        boat1.position.x -= boat1.speed*sin(boat1.rotation*M_PI/180);
        pool.position.x -= boat1.speed*sin(boat1.rotation*M_PI/180);
    }
        boat1.position.z -= boat1.speed*cos(boat1.rotation*M_PI/180);
        pool.position.z -= boat1.speed*cos(boat1.rotation*M_PI/180);

//        boat1.position.z = boat1.position.z -.5;
//        boat1.rotation = -4;
//        pool.position.z = pool.position.z - .5;
    }

    if (down){
//        camera_rotation_angle +=-1;
//    screen_zoom = screen_zoom + 2;
//        boat1.position.z = boat1.position.z +.05;
//        boat1.rotation = -4;
//        pool.position.z = pool.position.z + .05;
        if(boat1.position.x < 50)
        {
            boat1.position.x += boat1.speed*sin(boat1.rotation*M_PI/180);
            pool.position.x += boat1.speed*sin(boat1.rotation*M_PI/180);
        }

        boat1.position.z += boat1.speed*cos(boat1.rotation*M_PI/180);
        pool.position.z += boat1.speed*cos(boat1.rotation*M_PI/180);
    }

    if (space && !isup){
//        camera_rotation_angle +=-1;
//    screen_zoom = screen_zoom + 2;
        boat1.position.y += .01;
        boat1.speedy = 1;
        isup =1;
    }

    if (f & fire == 0)
    {
    //  cout << "pressing";
        fire = 1;
        fireball.speed = 3;
        fire_boss = 1;
    }
}

void tick_elements() {
    for(i=0;i<4;i++)
        heal[i].tick();
    for(i=0;i<2;i++)
        boost[i].tick();

    if(boat1.health < 00)
        quit(window);
    speed_camera();
    if (boat1.level)
        fireball.ssize = 2;
    boat1.existence = 1;
    if (!fire)
        fireball.set_position(boat1.position.x - 1*sin(boat1.rotation*M_PI/180) + 2,boat1.position.y + 2,boat1.position.z -4*cos(boat1.rotation*M_PI/180));
    if (fire)
    {
        fireball.position.x -= fireball.speed*sin(boat1.rotation*M_PI/180);
        fireball.position.z -= fireball.speed*cos(boat1.rotation*M_PI/180);
            if((boat1.position.x - fireball.position.x)*(boat1.position.x - fireball.position.x) +(boat1.position.z - fireball.position.z)*(boat1.position.z - fireball.position.z) > 1600)
                fire = 0;
    }

    if(boat1.position.y <= 30)
       {
            boat1.speedy = 0;
            boat1.position.y = 30;
            isup = 0;
//            boat1.tick();
       }
    for (i =0 ; i < 4 ; i++)
    {
        if (enemy[i].existence)
             enemy[i].tick();

    if(enemy[i].position.x > -30 && !enemyflag[i])
    {
        enemy[i].position.x -= enemy[i].speed;
//        enemyflag = 1;
    }
    else if(enemy[i].position.x < 30)
    {
        enemy[i].position.x += enemy[i].speed;
        enemyflag[i] = 1;
    }
    else
        enemyflag[i] = 0;

    if ((boat1.position.z - enemy[i].position.z < 20)&& enemy[i].existence)
    {
        boat1.position.z = enemy[i].position.z + 21 ;
        pool.position.z = enemy[i].position.z + 21;
    }

    if (detect_collision_enemy(fireball.bounding_box() , enemy[i].bounding_box()) && enemy[i].existence)
    {
        fire = 0;
        enemy[i].existence = 0;
        heal[i].existence = 1;
        boat1.level += .25;
        //update score here
    }

    }
    if (wind && boat1.position.z < -70 )
    {
        pool = Pool(boat1.position.x, 0 ,boat1.position.z ,COLOR_DEEP_BLUE);
        if (boat1.position.x > -49)
           {
            boat1.position.x -= .3;
//            boat1.position.z += .03;

        }
        if (wind && boat1.position.z < -105)
        {
            pool = Pool(boat1.position.x, 0 ,boat1.position.z ,COLOR_BLUE);
            wind = 0;
        }
    }
    if(enemy[0].existence == 0 && enemy[1].existence == 0 && enemy[2].existence == 0 && enemy[3].existence == 0 )
        bossflag = 1;

    if(bossflag)
    {
        if ((boat1.position.z - boss.position.z < 30)&& boss.existence)
        {
            boat1.position.z = boss.position.z + 31 ;
            pool.position.z = boss.position.z + 31;
          }
          if(boss.position.x > -30 && !bossflag1)
          {
                boss.position.x -= boss.speed;
          }
          else if(boss.position.x < 30)
            {
                boss.position.x += boss.speed;
                   bossflag1 = 1;
           }
            else
                bossflag1 = 0;

//          if(rand()%112 == 0)
//              fire_boss = 1;
          t  = atan((-boss.position.x + boat1.position.x)/(boat1.position.z - boss.position.z));
          if (!fire_boss)
              fireboat1.set_position(boss.position.x + 5*sin(boat1.rotation*M_PI/180) ,boss.position.y + 10,boss.position.z + 5*cos(boat1.rotation*M_PI/180));
          if (fire_boss)
          {
              fireboat1.position.x += fireboat1.speed*sin(t);
              fireboat1.position.z += fireboat1.speed*cos(t);
                  if((boss.position.x - fireboat1.position.x)*(boss.position.x - fireboat1.position.x) +(boss.position.z - fireball.position.z)*(boss.position.z - fireboat1.position.z) > 3600)
                      fire_boss = 0;
          }
          if (detect_collision_enemy(fireball.bounding_box() , boss.bounding_box()) && boss.existence)
          {
              fire = 0;
              if (boat1.level >=2)
                boss.health -=2;
              else
                 boss.health -= 1;
               if(boss.health == 0)
               {
                   boss.existence = 0;
               }
               score += 12;
              //update score here
          }

        boss.tick();
    }
    rock_collision = 0;
    boat1.tick();
    for (i=0;i<12;i++)
    star[i].tick();
    camera_rotation_angle +=0;
    boat1.rotation1 = 0;
    for(i = 0 ;i < 50 ; i++)
    {
     if (detect_collision(boat1.bounding_box() ,rock[i].bounding_box() ) && !rock_collision && rock[i].existence)
        {
         boat1.health -=1;
        rock[i].existence = 0;
        for(int k = 0 ; k < 15000001 ; k++)
        {
           if(!k%150000)
               boat1.existence = 1;
           else
            boat1.existence = 0;
        }
     }

     }
    for (i = 0 ; i < 12 ;i ++)
    {
    if(boat1.position.z - barrel[i].position.z <= 1 && boat1.position.z - barrel[i].position.z >-6 && barrel[i].position.y - boat1.position.y && barrel[i].existence)
    {
        if (boat1.position.x - barrel[i].position.x <= 2 && boat1.position.x - barrel[i].position.x >=0)
        {
            boat1.position.x = barrel[i].position.x + 2.05;
            pool.position.x = boat1.position.x;
        }
        else if (barrel[i].position.x - boat1.position.x <= 4 && barrel[i].position.x - boat1.position.x >=0)
        {
            boat1.position.x = barrel[i].position.x - 4.05;
            pool.position.x = boat1.position.x;
        }
       }
    }
    //       else if(boat1.position.x - barrel1.position.x < 1 && boat1.position.x - barrel1.position.x >0)
    for (i = 0 ; i < 12 ;i ++)
    {

    if(abs(boat1.position.z - star[i].position.z) <= 3 && abs(boat1.position.x - star[i].position.x) <=3 && star[i].position.y - boat1.position.y < 1 && star[i].existence)

    {
        star[i].existence  = 0;
        barrel[i].existence = 0;
        boat1.level += 1;
    }
    }

    for(i=0 ;i <4 ; i++)
    if(abs(boat1.position.z - heal[i].position.z) <= 3 && abs(boat1.position.x - heal[i].position.x) <=3  && heal[i].existence)
    {
        heal[i].existence  = 0;
        if(boat1.health < 90)
            boat1.health +=10;
        else
            boat1.health = 100;
//        barrel[i].existence = 0;
    }

    for (i=0;i<2;i++)
    if(abs(boat1.position.z - boost[i].position.z) <= 3 && abs(boat1.position.x - boost[i].position.x) <=3  && boost[i].existence)
    {
        boost[i].existence  = 0;
            boat1.speed +=1;
            boostflag[i] = 1;
    }

    sprintf(output,"Health: %d %d %f %f %f" ,(int)boat1.health , (int)boat1.level  , boat1.position.x, boat1.position.y, boat1.position.z);
    glfwSetWindowTitle(window,output);


}

/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL(GLFWwindow *window, int width, int height) {
    /* Objects should be created before any other gl function and shaders */
    // Create the models

//    boat1       = Ball(0, 0, COLOR_RED);
    boat1 = Boat(0 ,30 ,0 ,COLOR_BROWN );
    for (i = 0 ; i< 12 ; i++)
    {
        int x = -30 + rand()%60;
        int z = -10 - rand()%400;
    barrel[i] = Barrel( x ,31 ,z ,COLOR_YELLOW );
    star[i] = Star(x ,32 ,z ,COLOR_YELLOW);
    }
    for(i = 0 ;i < 50 ; i++)
    {
     GLint k =1;
     if (rand()%2 == 0)
         k = -1;

     rock[i] = Rock(k * rand()%50 ,33 , -rand()%500 ,COLOR_RED);
    }
    pool = Pool(0, 0 ,0 ,COLOR_BLUE);
    enemy[0]  = Enemy(0 , 31 , -20 , COLOR_PURPLE);
    enemy[1]  = Enemy(0 , 31 , -150 , COLOR_PINK);
    enemy[1].speed += 1;
    enemy[2]  = Enemy(0 , 31 , -300 , COLOR_PURPLE);
    enemy[2].speed += 2;

    enemy[3]  = Enemy(0 , 31 , -450 , COLOR_PINK);
    enemy[3].speed += 3;

    boss = Boss(0  , 33 , -500 , COLOR_YELLOW);

    fireball = Fireball(0 , 30 , 0 ,1 , COLOR_FIRE);
    fireboat1 = Fireball(0 , 0 , 0 ,5, COLOR_GREEN);
    fireboat1.speed +=1;
    fireboat1.rotation = 45;
    for(i=0;i<2;i++)
        boost[i] = Boost(rand()%10 - 5 , 31 , -rand()%100 - 125 ,COLOR_BOOST);
    for(i=0;i<4;i++)
    {
        heal[i] = Heal(enemy[i].position.x , enemy[i].position.y , enemy[i].position.z ,COLOR_LIME);
        heal[i].existence = 0;
    }
        // Create and compile our GLSL program from the shaders
    programID = LoadShaders("Sample_GL.vert", "Sample_GL.frag");
    // Get a handle for our "MVP" uniform
    Matrices.MatrixID = glGetUniformLocation(programID, "MVP");


    reshapeWindow (window, width, height);

    // Background color of the scene
    glClearColor (COLOR_BACKGROUND.r / 256.0, COLOR_BACKGROUND.g / 256.0, COLOR_BACKGROUND.b / 256.0, 0.0f); // R, G, B, A
    glClearDepth (1.0f);

    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);

    cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
    cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
    cout << "VERSION: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}


int main(int argc, char **argv) {
    srand(time(0));
    int width  = 600;
    int height = 600;
    int count[2] = {0};
    window = initGLFW(width, height);

    initGL (window, width, height);

    /* Draw in loop */
    while (!glfwWindowShouldClose(window)) {
        // Process timers

        if (t60.processTick()) {
            // 60 fps
            // OpenGL Draw commands
            draw();
            for(i=0;i<2;i++)
            {
//             cout << count[i] <<endl;
            if(boostflag[i])
                count[i] += 1;
            if(count[i] > 500)
                {
                boostflag[i] = 0;
                count[i] = 0;
                boat1.speed -= 1;
                }
            }
                // Swap Frame Buffer in double buffering
            glfwSwapBuffers(window);

            tick_elements();
            tick_input(window);
        }

        // Poll for Keyboard and mouse events
        glfwPollEvents();
    }

    quit(window);
}

bool detect_collision(bounding_box_t a, bounding_box_t b) {
    return ((((b.z - a.z)  < (a.depth + b.depth/2)) && b.z -a.z > 0) || (((a.z - b.z)  < (2 + b.depth/2))&& a.z -b.z > 0))&&
//            (abs(a.y - b.y) * 2 < (a.height + b.height))&&
            (abs(a.x - b.x) * 2 < (a.width + b.width));

}

bool detect_collision_barrel(bounding_box_t a, bounding_box_t b) {
    return (((((b.z - a.z)<=1.1)&& (b.z-a.z >0))||(a.z -b.z <= 6 && a.z -b.z >0))&& (((a.x - b.x < 2) && (a.x -b.x > 0)) || (b.x - a.x < 4 && b.x - a.x >0 )) &&  (a.y - b.y) < 0);
}

bool detect_collision_enemy(bounding_box_t a, bounding_box_t b) {
    return  ((abs(a.x - b.x) * 2 < (a.width + b.width))) && (abs(a.z - b.z) * 2 < (a.depth + b.depth)) ;
}

//bool detect_collision_star(bounding_box_t a, bounding_box_t b) {
//    return  ((b.x - a.x)  < (a.width + b.width/2) && b.x-a.x>0) && (abs(a.z - b.z) < .01) && (a.y - b.y <=.1);
//}
bool detect_collision_star(bounding_box_t a, bounding_box_t b) {
    return  (abs(a.x - b.x) * 2 < (a.width + b.width))&&
            (abs(a.y - b.y) * 2 < (a.height + b.height))&&
            (abs(a.z - b.y) * 2 < (a.depth + b.depth));

}

void reset_screen() {
//    float top    = screen_center_y + 24 / screen_zoom;
//    float bottom = screen_center_y - 24 / screen_zoom;
//    float left   = screen_center_x - 24 / screen_zoom;
//    float right  = screen_center_x + 24 / screen_zoom;
//    Matrices.projection = glm::ortho(left, right, bottom, top, 0.1f, 500.0f);
//    if (view == 0)
//          Matrices.projection = glm::perspective(45.0f, 1.0f, 5.0f, 150.0f);

//    else if(view == 1)
//        Matrices.projection = glm::perspective(45.0f, 1.0f, 10.0f, 150.0f);

//    else if(view == 2)
        Matrices.projection = glm::perspective(45.0f, 1.0f, 10.0f, 150.0f);

//    else if (view == 3)
//      Matrices.projection = glm::perspective(45.0f, 1.0f, 10.0f, 150.0f);

}

void camera_change()
{
    view = (view+1)%5;
}

void speed_camera()
{
//    cout << view << endl;
    if(view==0){
//follow
//        x_c=boat1.position.x+75*sin(boat1.rotation*M_PI/180.0f);
//        y_c=35;
//        z_c=boat1.position.z+75*cos(boat1.rotation*M_PI/180.0f);
        camera_x = boat1.position.x+75*sin(boat1.rotation*M_PI/180.0f);;
        camera_y = 35;
        camera_z = boat1.position.z+75*cos(boat1.rotation*M_PI/180.0f);
        target_x = boat1.position.x;
        target_y = boat1.position.y;
        target_z = boat1.position.z;

    }
    else if(view==1)
    {
        double theta = (boat1.rotation)*(M_PI/180);

        camera_x = boat1.position.x+3*sin(theta);
        camera_y = boat1.position.y+10;
        camera_z = boat1.position.z+3*cos(theta);

        target_x = boat1.position.x-10*sin(theta);
        target_y = boat1.position.y+10;
        target_z = boat1.position.z-10*cos(theta);

    }
    else if(view==2)
    {
        // tower view
        camera_y = 50;
        camera_z = 45;
//        camera_z+=5;
//        target_y = 30;
        target_x = boat1.position.x;
        target_y = boat1.position.y;
        target_z = boat1.position.z;
    }
    else if(view==3)
    {
        // top view
        camera_x = boat1.position.x;
        camera_y = 100;
        camera_z = boat1.position.z;

        target_x = boat1.position.x+1;
        target_y = boat1.position.y;
        target_z = boat1.position.z;
//        printf("cx=%f cy=%f cz=%f\n", camera_x, camera_y, camera_z);
//        printf("tx=%f ty=%f tz=%f\n", target_x, target_y, target_z);
    }
    else if(view==4)
    {
        // helicopter
        camera_x = boat1.position.x+90;
        camera_y = 90;
        camera_z = boat1.position.z;

        target_x = boat1.position.x;
        target_y = boat1.position.y;
        target_z = boat1.position.z;
    }

}

void heli_camera(float x, float y)
{
    if(view==2)
    {
        target_x = boat1.position.x+(x-300);
        if(y<=300)
        {
            target_y = boat1.position.y+(300-y)/2;
        }
    }
}

void zoom_camera(int type)
{
    if(view==2)
    {
        double l = target_x-camera_x;
        double m = target_y-camera_y;
        double n = target_z-camera_z;
        if(type==1)
        {
            if(camera_z-10>target_z)
                camera_z-=10;
        }
        else if(type==-1)
        {
            camera_z+=10;
        }
        camera_x = l*(camera_z-target_z)/n+target_x;
        camera_y = m*(camera_z-target_z)/n+target_y;
    }
}
