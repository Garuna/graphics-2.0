#include "pool.h"
#include "main.h"

Pool::Pool(float x, float y ,float z , color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 0;
    this->speed = .01;
//    this->level = 2;
    this->existence = 1;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices

    static const GLfloat vertex_buffer_data[] = {
        -1.0f,-1.0f,-1.0f, // triangle 1 : begin
        -1.0f,-1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f, // triangle 1 : end

        1.0f, 1.0f,-1.0f, // triangle 2 : begin
        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f, // triangle 2 : end


        1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,

        1.0f, 1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,


        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f,-1.0f,

        1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f,-1.0f,


        -1.0f, 1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
        1.0f,-1.0f, 1.0f,

        1.0f, 1.0f, 1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f,-1.0f,


        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f,-1.0f, 1.0f,

        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f,


        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,

        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f,-1.0f, 1.0f
    };
    GLint i=0,j=0;

    GLfloat g_vertex_buffer_data[20000];

        for(i= 0 ; i < 12*9 ; i++){
            g_vertex_buffer_data[j++] = 30*vertex_buffer_data[i];
        }

//    const color_t color2 = { 135, 211, 124 };

    this->object = create3DObject(GL_TRIANGLES, 20*9, g_vertex_buffer_data, color, GL_FILL );
//    this->object1 = create3DObject(GL_TRIANGLES, 1220*3, g_vertex_buffer_data1,color1, GL_FILL );
//    this->object2 = create3DObject(GL_TRIANGLES, 1220*3, g_vertex_buffer_data2, color2, GL_LINE );
//    this->object3 = create3DObject(GL_TRIANGLES, 1220*3, g_vertex_buffer_data3,color1, GL_LINE);

}

void Pool::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 1, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
//    draw3DObject(this->object1);
//    draw3DObject(this->object2);
//    draw3DObject(this->object3);

}

void Pool::set_position(float x, float y , float z) {
    this->position = glm::vec3(x, y, z);
}

void Pool::tick() {
//    this->rotation += speed;
    this->position.z -= this->speed;
    // this->position.y -= speed;
}

