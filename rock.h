#include "main.h"

#ifndef Rock_H
#define Rock_H


class Rock {
public:
    Rock() {}
    Rock(float x,float y ,float z, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    void tick();
    double speed;
    bool existence;
    bounding_box_t bounding_box();
private:
    VAO *object;
    VAO * object1;
    VAO * object2;
    VAO * object3;

};

#endif // Rock_H
