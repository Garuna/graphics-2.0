#include "main.h"

#ifndef Fireball_H
#define Fireball_H


class Fireball {
public:
    Fireball() {}
    Fireball(float x, float y,float z,float size , color_t color);
    glm::vec3 position;
    float rotation;
    float rotation1;
    void draw(glm::mat4 VP);
    void set_position(float x, float y ,float z);
    double ssize;
    void tick();
    double speed;
    double speedx ;
    double level;
    bool existence;
    bounding_box_t bounding_box();
    double health;
private:
    VAO *object;
    VAO * object1;
    VAO * object2;
    VAO * object3;

};

#endif // Fireball_H
