#include "boat.h"
#include "main.h"

Boat::Boat(float x, float y,float z, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 0;
    this->rotation1 = 0;
    this->speed = 1;
    this->existence = 1;
    this->level = 0;
    this->health = 100;
    this->speedy = 0;
    this->counter = 0;
    this->wave = .1;
    this->blink = 0;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices

    GLfloat vertex_buffer_data[] = {
        -1.0f,-1.0f,-1.0f, // triangle 1 : begin
        -1.0f,-1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f, // triangle 1 : end

        1.0f, 1.0f,-1.0f, // triangle 2 : begin
        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f, // triangle 2 : end


        1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,

        1.0f, 1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,


        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f,-1.0f,

        1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f,-1.0f,


        -1.0f, 1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
        1.0f,-1.0f, 1.0f,

        1.0f, 1.0f, 1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f,-1.0f,


        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f,-1.0f, 1.0f,

        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f,


        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,

        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f,-1.0f, 1.0f
    };

    GLint i=0,j=0 , temp = 0;

//    for (i = 0; i < 12*9 -1; i++){
//        if (i%3 == 1)
//        {
//            temp = vertex_buffer_data[i];
//            vertex_buffer_data[i] = vertex_buffer_data[i+1];
//        }
//        else if (i%3 == 2)
//            vertex_buffer_data[i] = temp;
//    }
//    vertex_buffer_data[i] = temp;

    GLfloat g_vertex_buffer_data[200000];
    GLfloat g_vertex_buffer_data1[200000];
    GLfloat g_vertex_buffer_data2[200000];
    GLfloat g_vertex_buffer_data3[200000];
    GLfloat g_vertex_buffer_data4[200000];

        for(i= 0 ; i < 12*9 ; i++){
            g_vertex_buffer_data[j++] = vertex_buffer_data[i];
        }
        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 ==2)
                g_vertex_buffer_data[j++] = 2 + vertex_buffer_data[i];
            else
                g_vertex_buffer_data[j++] = vertex_buffer_data[i];
        }
        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 ==2)
                g_vertex_buffer_data[j++] = 4 + vertex_buffer_data[i];
            else
                g_vertex_buffer_data[j++] = vertex_buffer_data[i];
        }
        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 == 0 )
            g_vertex_buffer_data[j++] = 2 + vertex_buffer_data[i];
            else
                g_vertex_buffer_data[j++] = vertex_buffer_data[i];

        }
        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 == 0 )
            g_vertex_buffer_data[j++] = 2 + vertex_buffer_data[i];

            else if (i%3 ==2)
                g_vertex_buffer_data[j++] = 2 + vertex_buffer_data[i];
            else
                g_vertex_buffer_data[j++] = vertex_buffer_data[i];
        }
        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 == 0 )
                g_vertex_buffer_data[j++] = 2 + vertex_buffer_data[i];

            else if (i%3 ==2)
                g_vertex_buffer_data[j++] = 4 + vertex_buffer_data[i];
            else
                g_vertex_buffer_data[j++] = vertex_buffer_data[i];
        }
        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 ==2)
                g_vertex_buffer_data4[j++] = -2 + .5*vertex_buffer_data[i];
            else if (i%3 == 1)
                g_vertex_buffer_data4[j++] = 2 + .5*vertex_buffer_data[i];
            else
                g_vertex_buffer_data4[j++] = 2.5 + .5*vertex_buffer_data[i];
        }

        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 ==2)
                g_vertex_buffer_data4[j++] = -2 + .5*vertex_buffer_data[i];
            else if (i%3 == 1)
                g_vertex_buffer_data4[j++] = 2 + .5*vertex_buffer_data[i];
            else
                g_vertex_buffer_data4[j++] = -.5 + .5*vertex_buffer_data[i];
        }
        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 ==2)
                g_vertex_buffer_data3[j++] = -2 + vertex_buffer_data[i];
            else if (i%3 == 1)
                g_vertex_buffer_data3[j++] = 2 + .7*vertex_buffer_data[i];
            else
                g_vertex_buffer_data3[j++] = 1+ vertex_buffer_data[i];
        }

        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 ==2)
                g_vertex_buffer_data1[j++] = 2 + .1*vertex_buffer_data[i];
            else if (i%3 == 1)
                g_vertex_buffer_data1[j++] = 4 + 4*vertex_buffer_data[i];
            else
                g_vertex_buffer_data1[j++] = 1 + .1*vertex_buffer_data[i];

        }

        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 ==2)
                g_vertex_buffer_data2[j++] = 4 + .7*vertex_buffer_data[i];
            else if (i%3 == 1)
                g_vertex_buffer_data2[j++] =  .7*vertex_buffer_data[i];
            else
                g_vertex_buffer_data2[j++] = -2 + .7*vertex_buffer_data[i];

        }
        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 ==2)
                g_vertex_buffer_data2[j++] = 4 + .7*vertex_buffer_data[i];
            else if (i%3 == 1)
                g_vertex_buffer_data2[j++] =  .7*vertex_buffer_data[i];
            else
                g_vertex_buffer_data2[j++] = -2*.7 + .7*vertex_buffer_data[i] + 5.4;

        }
        for(i= 0 ; i < 12*9 ; i++){
            if (i%3 ==2)
                g_vertex_buffer_data[j++] = 4 + 2*.6 + .6*vertex_buffer_data[i];
            else if (i%3 == 1)
                g_vertex_buffer_data[j++] = 1+ .6*vertex_buffer_data[i];
            else
                g_vertex_buffer_data[j++] = 1 + .6*vertex_buffer_data[i];

        }

    const color_t color1 = { 52, 73, 94 };
    const color_t color2 = { 135, 211, 124 };

    this->object = create3DObject(GL_TRIANGLES, 1220*3, g_vertex_buffer_data, color, GL_FILL );
    this->object1 = create3DObject(GL_TRIANGLES, 1220*3, g_vertex_buffer_data1,color1, GL_FILL );
    this->object2 = create3DObject(GL_TRIANGLES, 1220*3, g_vertex_buffer_data2, color2, GL_FILL );
    this->object3 = create3DObject(GL_TRIANGLES, 1220*3, g_vertex_buffer_data3,color1, GL_LINE );
    this->object4 = create3DObject(GL_TRIANGLES, 1220*3, g_vertex_buffer_data4,color1, GL_LINE );

}

void Boat::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 1, 0));
//    glm::mat4 rotate    = glm::rotate((float) (this->rotation1 * M_PI / 180.0f), glm::vec3(0, 1, 0));

    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
//    rotate    = glm::rotate((float) (this->rotation1 * M_PI / 180.0f), glm::vec3(0, 1, 0));
//    Matrices.model *= rotate;
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);

        this->counter = 0;
        this->blink = 0;
        draw3DObject(this->object);
        draw3DObject(this->object1);
        draw3DObject(this->object2);
        draw3DObject(this->object3);

        if (this->level >=2)
            draw3DObject(this->object4);
}

void Boat::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Boat::tick() {
//    this->rotation += speed;
//     this->position.z -= this->speed;
     this->position.y += this->speedy;
     this->speedy -= .1;
    // this->position.y -= speed;
}

bounding_box_t Boat::bounding_box() {
    float x = this->position.x, y = this->position.y ,z = this->position.z ;
    bounding_box_t bbox = { x, y,z, 4 , 8 , 5};
    return bbox;
}

