#include "main.h"

#ifndef Boat_H
#define Boat_H


class Boat {
public:
    Boat() {}
    Boat(float x, float y,float z, color_t color);
    glm::vec3 position;
    float rotation;
    float rotation1;
    void draw(glm::mat4 VP);
    void set_position(float x, float y ,float z);
    void tick();
    bool blink;
    bool counter;
    double speed;
    double speedy ;
    double level;
    bool existence;
    bounding_box_t bounding_box();
    double health;
    double wave;
private:
    VAO *object;
    VAO * object1;
    VAO * object2;
    VAO * object3;
    VAO * object4;

};

#endif // Boat_H
